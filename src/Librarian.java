
/**********************************************
 * CS 485: Database Sys Design
 * Project 1: Library Database
 * Authors: Nick Horn & William Newshutz
 * 
 * File: Librarian.java
 * Desc: Contains all methods needed for a
 * 		 Librarian to access and manipulate
 * 		 the Library database.
 *********************************************
 */

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;

public class Librarian {

	private Connection connect = null;
    private Statement statement = null;
    private ResultSet resultSet = null;
    private PreparedStatement preparedStatement = null;
    private String SQLUSER = "yk9579va";
    private String SQLPASS = "Junkrat";
    private String DBName = "yk9579va_Library";
    private String DBConnection = "jdbc:mysql://localhost/"+DBName+"?user="+ SQLUSER +"&password="+ SQLPASS;
	
	/**********************
	 *                    *
	 *  Security Methods  *
	 *                    *
	 **********************
	 */
    
    //start validating password
    public boolean validatePassword(String password) {
		Boolean result = false;
		String query = "SELECT Login.salt, Login.hash "
					 + "FROM Login WHERE Login.userID = 1";
				
		try {
            // This will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.jdbc.Driver");
            // Setup the connection with the DB
            connect = DriverManager.getConnection(DBConnection);
            
            
            preparedStatement = connect.prepareStatement(query);
            //preparedStatement.setInt(1, 1);
            //System.out.println(preparedStatement.toString());
            resultSet = preparedStatement.executeQuery();
            
            if(resultSet.next()) {
            	//System.out.println("Hi");
            }
            
            String saltString = resultSet.getString("Login.salt");
            //System.out.println(saltString);
            byte[] saltBytes = new byte[16];
            for(int ii = 0; ii < saltString.length(); ii += 2) {
         	   	saltBytes[ii / 2] = (byte) ((Character.digit(saltString.charAt(ii), 16) << 4)
                         + Character.digit(saltString.charAt(ii+1), 16));
            }
            
            result = get_SHA_512_SecurePassword(password, saltBytes, resultSet.getString("Login.hash"));
            
        } catch(SQLException s) {
        	
        } catch (Exception e) {
            
        } finally {
            close();
        }
		
		return result;
	}//end validatePassword
    
    //compare hash(password+salt) vs stored hash
    private static boolean get_SHA_512_SecurePassword(String passwordToHash, byte[] salt, String hash)
    {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(salt);
            byte[] bytes = md.digest(passwordToHash.getBytes());
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return generatedPassword.equals(hash);
    }//end comparing hash
    
    
    public void connectionTest() {
    	String query = "Select * FROM Authors;";
    	
    	try {
    		// This will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.jdbc.Driver");
            // Setup the connection with the DB
            connect = DriverManager.getConnection(DBConnection);
            
            preparedStatement = connect.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
    	}
    	catch(SQLException s) {
        	System.out.println("Connection to database failed:: " + s);
        } catch (Exception e) {
        	System.out.println("Connection to database failed:: " + e);
        } finally {
            close();
        }
    	
    	System.out.println("Connection Successful\n");
    }
    
	/******************
	 *                *
	 *  Book Methods  *
	 *                *
	 ******************
	 */
	
	public String searchBooksByID(int bookID) {
		String result = "";
		String query = "SELECT Books.bookID, Books.title, Books.ISBN, Books.publicationDate, "
					 + "Books.inOutStatus, Authors.fName, Authors.lName, Genres.genreName "
					 + "FROM Books, Authors, AuthorToISBN, Genres WHERE Books.bookID = ? "
					 + "AND Books.ISBN = AuthorToISBN.ISBN && Authors.authorID = AuthorToISBN.authorID "
					 + "AND Books.genreID = Genres.genreID;";
		
		try {
            // This will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.jdbc.Driver");
            // Setup the connection with the DB
            connect = DriverManager.getConnection(DBConnection);
            
            preparedStatement = connect.prepareStatement(query);
            preparedStatement.setInt(1, bookID);
            resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
            	result += "Title:   " + resultSet.getString("Books.title") 
            			+ "\nID:      " + resultSet.getInt("Books.bookID")
            			+ "\nISBN:    " + resultSet.getInt("Books.ISBN")
            			+ "\nPubDate: " + resultSet.getString("publicationDate")
            			+ "\nAuthor:  " + resultSet.getString("Authors.fName") + " " + resultSet.getString("Authors.lName")
            			+ "\nGenre:   " + resultSet.getString("Genres.genreName"); 
            	if(resultSet.getInt("Books.inOutStatus") == 0) {
            		result += "\nStatus:  In Stock\n\n";
            	}
            	else {
            		result += "\nStatus:  Checked Out\n\n";
            	}
            }
        } catch(SQLException s) {
        	
        } catch (Exception e) {
        	
        } finally {
            close();
        }
		
		return result;
	}//end searchBooksByID
	
	public String searchBooksByISBN(int ISBN) {
		String result = "";
		String query = "SELECT Books.bookID, Books.title, Books.ISBN, Books.publicationDate, "
					 + "Books.inOutStatus, Authors.fName, Authors.lName, Genres.genreName "
					 + "FROM Books, Authors, AuthorToISBN, Genres WHERE Books.ISBN = ? "
					 + "AND Books.ISBN = AuthorToISBN.ISBN && Authors.authorID = AuthorToISBN.authorID "
					 + "AND Books.genreID = Genres.genreID ORDER BY Books.bookID;";
		
		try {
            // This will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.jdbc.Driver");
            // Setup the connection with the DB
            connect = DriverManager.getConnection(DBConnection);
            
            preparedStatement = connect.prepareStatement(query);
            preparedStatement.setInt(1, ISBN);
            resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
            	result += "Title:   " + resultSet.getString("Books.title") 
            			+ "\nID:      " + resultSet.getInt("Books.bookID")
            			+ "\nISBN:    " + resultSet.getInt("Books.ISBN")
            			+ "\nPubDate: " + resultSet.getString("publicationDate")
            			+ "\nAuthor:  " + resultSet.getString("Authors.fName") + " " + resultSet.getString("Authors.lName")
            			+ "\nGenre:   " + resultSet.getString("Genres.genreName"); 
            	if(resultSet.getInt("Books.inOutStatus") == 0) {
            		result += "\nStatus:  In Stock\n\n";
            	}
            	else {
            		result += "\nStatus:  Checked Out\n\n";
            	}
            }
        } catch(SQLException s) {
        	
        } catch (Exception e) {
            
        } finally {
            close();
        }
		
		return result;
	}//end searchBooksByISBN
	
	public String searchBooksByGenreID(int genreID) {
		String result = "";
		String query = "SELECT Books.bookID, Books.title, Books.ISBN, Books.publicationDate, "
					 + "Books.inOutStatus, Authors.fName, Authors.lName, Genres.genreName "
					 + "FROM Books, Authors, AuthorToISBN, Genres WHERE Books.genreID = ? "
					 + "AND Books.ISBN = AuthorToISBN.ISBN && Authors.authorID = AuthorToISBN.authorID "
					 + "AND Books.genreID = Genres.genreID ORDER BY Books.title;";
		
		try {
            // This will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.jdbc.Driver");
            // Setup the connection with the DB
            connect = DriverManager.getConnection(DBConnection);
            
            preparedStatement = connect.prepareStatement(query);
            preparedStatement.setInt(1, genreID);
            resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
            	result += "Title:   " + resultSet.getString("Books.title") 
            			+ "\nID:      " + resultSet.getInt("Books.bookID")
            			+ "\nISBN:    " + resultSet.getInt("Books.ISBN")
            			+ "\nPubDate: " + resultSet.getString("publicationDate")
            			+ "\nAuthor:  " + resultSet.getString("Authors.fName") + " " + resultSet.getString("Authors.lName")
            			+ "\nGenre:   " + resultSet.getString("Genres.genreName"); 
            	if(resultSet.getInt("Books.inOutStatus") == 0) {
            		result += "\nStatus:  In Stock\n\n";
            	}
            	else {
            		result += "\nStatus:  Checked Out\n\n";
            	}
            }
        } catch(SQLException s) {
        	
        } catch (Exception e) {
            
        } finally {
            close();
        }
		
		return result;
	}//end searchBooksByGenreID
	
	public String searchBooksByTitle(String title) {
		String result = "";
		String query = "SELECT Books.bookID, Books.title, Books.ISBN, Books.publicationDate, "
					 + "Books.inOutStatus, Authors.fName, Authors.lName, Genres.genreName "
					 + "FROM Books, Authors, AuthorToISBN, Genres WHERE Books.title LIKE ? "
					 + "AND Books.ISBN = AuthorToISBN.ISBN && Authors.authorID = AuthorToISBN.authorID "
					 + "AND Books.genreID = Genres.genreID ORDER BY Books.title;";
		
		try {
            // This will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.jdbc.Driver");
            // Setup the connection with the DB
            connect = DriverManager.getConnection(DBConnection);
            
            preparedStatement = connect.prepareStatement(query);
            preparedStatement.setString(1, "%"+title+"%");
            resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
            	result += "Title:   " + resultSet.getString("Books.title") 
            			+ "\nID:      " + resultSet.getInt("Books.bookID")
            			+ "\nISBN:    " + resultSet.getInt("Books.ISBN")
            			+ "\nPubDate: " + resultSet.getString("publicationDate")
            			+ "\nAuthor:  " + resultSet.getString("Authors.fName") + " " + resultSet.getString("Authors.lName")
            			+ "\nGenre:   " + resultSet.getString("Genres.genreName"); 
            	if(resultSet.getInt("Books.inOutStatus") == 0) {
            		result += "\nStatus:  In Stock\n\n";
            	}
            	else {
            		result += "\nStatus:  Checked Out\n\n";
            	}
            }
        } catch(SQLException s) {
        	System.out.println(s);
        } catch (Exception e) {
        	System.out.println(e);
        } finally {
            close();
        }
		
		return result;
	}//end searchBooksByTitle
	
	public String searchBooksByAuthorID(int authID) {
		String result = "";
		String query = "SELECT Books.bookID, Books.title, Books.ISBN, Books.publicationDate, "
					 + "Books.inOutStatus, Authors.fName, Authors.lName, Genres.genreName "
					 + "FROM Books, Authors, AuthorToISBN, Genres WHERE Authors.authorID = ? "
					 + "AND Books.ISBN = AuthorToISBN.ISBN && Authors.authorID = AuthorToISBN.authorID "
					 + "AND Books.genreID = Genres.genreID ORDER BY Books.title;";
		
		try {
            // This will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.jdbc.Driver");
            // Setup the connection with the DB
            connect = DriverManager.getConnection(DBConnection);
            
            preparedStatement = connect.prepareStatement(query);
            preparedStatement.setInt(1, authID);
            resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
            	result += "Title:   " + resultSet.getString("Books.title") 
            			+ "\nID:      " + resultSet.getInt("Books.bookID")
            			+ "\nISBN:    " + resultSet.getInt("Books.ISBN")
            			+ "\nPubDate: " + resultSet.getString("publicationDate")
            			+ "\nAuthor:  " + resultSet.getString("Authors.fName") + " " + resultSet.getString("Authors.lName")
            			+ "\nGenre:   " + resultSet.getString("Genres.genreName"); 
            	if(resultSet.getInt("Books.inOutStatus") == 0) {
            		result += "\nStatus:  In Stock\n\n";
            	}
            	else {
            		result += "\nStatus:  Checked Out\n\n";
            	}
            }
        } catch(SQLException s) {
        	
        } catch (Exception e) {
            
        } finally {
            close();
        }
		
		return result;
	}//end searchBooksByAuthorID
	
	public String searchBooksByAuthorLName(String authLName) {
		String result = "";
		String query = "SELECT Books.bookID, Books.title, Books.ISBN, Books.publicationDate, "
					 + "Books.inOutStatus, Authors.fName, Authors.lName, Genres.genreName "
					 + "FROM Books, Authors, AuthorToISBN, Genres WHERE Authors.lName LIKE ? "
					 + "AND Books.ISBN = AuthorToISBN.ISBN && Authors.authorID = AuthorToISBN.authorID "
					 + "AND Books.genreID = Genres.genreID ORDER BY Books.title;";
		
		try {
            // This will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.jdbc.Driver");
            // Setup the connection with the DB
            connect = DriverManager.getConnection(DBConnection);
            
            preparedStatement = connect.prepareStatement(query);
            preparedStatement.setString(1, "%" + authLName + "%");
            resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
            	result += "Title:   " + resultSet.getString("Books.title") 
            			+ "\nID:      " + resultSet.getInt("Books.bookID")
            			+ "\nISBN:    " + resultSet.getInt("Books.ISBN")
            			+ "\nPubDate: " + resultSet.getString("publicationDate")
            			+ "\nAuthor:  " + resultSet.getString("Authors.fName") + " " + resultSet.getString("Authors.lName")
            			+ "\nGenre:   " + resultSet.getString("Genres.genreName"); 
            	if(resultSet.getInt("Books.inOutStatus") == 0) {
            		result += "\nStatus:  In Stock\n\n";
            	}
            	else {
            		result += "\nStatus:  Checked Out\n\n";
            	}
            }
        } catch(SQLException s) {
        	
        } catch (Exception e) {
            
        } finally {
            close();
        }
		
		return result;
	}//end searchBooksByAuthorLName
	
	public String searchBooksByReleaseDate(String date) {
		String result = "";
		String query = "SELECT Books.bookID, Books.title, Books.ISBN, Books.publicationDate, "
					 + "Books.inOutStatus, Authors.fName, Authors.lName, Genres.genreName "
					 + "FROM Books, Authors, AuthorToISBN, Genres WHERE Books.publicationDate LIKE ? "
					 + "AND Books.ISBN = AuthorToISBN.ISBN && Authors.authorID = AuthorToISBN.authorID "
					 + "AND Books.genreID = Genres.genreID ORDER BY Books.title;";
		
		try {
            // This will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.jdbc.Driver");
            // Setup the connection with the DB
            connect = DriverManager.getConnection(DBConnection);
            
            preparedStatement = connect.prepareStatement(query);
            preparedStatement.setString(1, "%"+date+"%");
            resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
            	result += "Title:   " + resultSet.getString("Books.title") 
            			+ "\nID:      " + resultSet.getInt("Books.bookID")
            			+ "\nISBN:    " + resultSet.getInt("Books.ISBN")
            			+ "\nPubDate: " + resultSet.getString("publicationDate")
            			+ "\nAuthor:  " + resultSet.getString("Authors.fName") + " " + resultSet.getString("Authors.lName")
            			+ "\nGenre:   " + resultSet.getString("Genres.genreName"); 
            	if(resultSet.getInt("Books.inOutStatus") == 0) {
            		result += "\nStatus:  In Stock\n\n";
            	}
            	else {
            		result += "\nStatus:  Checked Out\n\n";
            	}
            }
        } catch(SQLException s) {
        	
        } catch (Exception e) {
            
        } finally {
            close();
        }
		
		return result;
	}//end searchBooksByReleaseDate
	
	
		//id auto-increments, inOutStatus defaults true.
	public boolean addNewBook(int ISBN, String title, int genreID, int authorID, String pubDate) {
		String query1 = "INSERT INTO Books (bookID, ISBN, title, genreID, publicationDate, inOutStatus) "
					 + "VALUES (NULL, ?, ?, ?, ?, 0);";
		
		String query2 = "INSERT INTO AuthorToISBN (authorID, ISBN) VALUES (?, ?);";
	
		boolean auth2ISBN = false;
		boolean idCheck = true;
		try {
	       // This will load the MySQL driver, each DB has its own driver
	       Class.forName("com.mysql.jdbc.Driver");
	       // Setup the connection with the DB
	       connect = DriverManager.getConnection(DBConnection);
	       
	       //prepare and run query1 : create book
	       preparedStatement = connect.prepareStatement(query1);
	       preparedStatement.setInt(1, ISBN);
	       preparedStatement.setString(2, title);
	       preparedStatement.setInt(3, genreID);
	       preparedStatement.setString(4, pubDate);
	       int flag = preparedStatement.executeUpdate();
		      if(flag == 0) {
		    	  throw new Exception();
		      }
	
	       //prepare and run query2 : link Author and ISBN
	       //throws exception if link exists, but exception is caught and ignored since this is okay
	       preparedStatement = connect.prepareStatement(query2);
	       preparedStatement.setInt(1, authorID);
	       preparedStatement.setInt(2, ISBN);
	       preparedStatement.executeUpdate();
	       
	       auth2ISBN = true;
	       idCheck = false;
	       
		} catch(SQLException s) {
	   		if(auth2ISBN == true) {
	   			return false;
	   		}
	   		if(idCheck == true) {
	   			return false;
	   		}
		} catch (Exception e) {
	       return false;
		} finally {
	       close();
		}
		
		return true;
	}//end addNewBook
	
	/*
	 *  Removed delete book functionality for now.  We do not feel comfortable yet cascading changes
	 *  and restricting changes makes this method not useful.  For now we have decided this kind of change should
	 *  be completed by a SysAdmin on the database directly. 
	 *
		//returns true if removing book successful, else false
	public boolean decomissionBookByID(int id) {
		String query = "DELETE FROM Books WHERE bookID = ?;";
	
		try {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager.getConnection(DBConnection);
	      
	      preparedStatement = connect.prepareStatement(query);
	      preparedStatement.setInt(1, id);
	      int flag = preparedStatement.executeUpdate();
	      if(flag == 0) {
	    	  throw new Exception();
	      }
	      
		} catch(SQLException s) {
	  		return false;
		} catch (Exception e) {
			return false;
		} finally {
	      close();
		}

		return true;
	}//end decomissionBookByID
	*
	*/
	
	
		//returns true if edit successful, else false
	public boolean editBookISBN(int id, int ISBN) {
		String query = "UPDATE Books SET ISBN = ? WHERE bookID = ?;";
		String query2 = "INSERT INTO AuthorToISBN (authorID, ISBN) VALUES (?, ?);";
		String query3 = "SELECT AuthorToISBN.authorID FROM Books, AuthorToISBN WHERE Books.bookID = ? "
					  + "AND Books.ISBN = AuthorToISBN.ISBN;";
		int authID = -1;
		boolean idCheck = true;
		boolean auth2ISBN = false;
		
		try {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager.getConnection(DBConnection);
	      
	      preparedStatement = connect.prepareStatement(query3);
	      preparedStatement.setInt(1, id);
	      resultSet = preparedStatement.executeQuery();
	      if(resultSet.next()) {
	    	  authID = resultSet.getInt("AuthorToISBN.authorID");
	      }
	      preparedStatement = connect.prepareStatement(query);
	      preparedStatement.setInt(1, ISBN);
	      preparedStatement.setInt(2, id);
	      int flag = preparedStatement.executeUpdate();
	      if(flag == 0) {
	    	  throw new Exception();
	      }
	      
	      idCheck = false;
	      
	      preparedStatement = connect.prepareStatement(query2);
	      preparedStatement.setInt(1, authID);
	      preparedStatement.setInt(2, ISBN);
	      preparedStatement.executeUpdate();
	      
	      auth2ISBN = true;
	      
		} catch(SQLException s) {
			//System.out.print(s);
			if(auth2ISBN == true) {
	   			return false;
	   		}
	   		if(idCheck == true) {
	   			return false;
	   		}
		} catch (Exception e) {
			return false;
		} finally {
	      close();
		}

		return true;
	}//end editBookISBN
	
		//returns true if edit successful, else false
	public boolean editBookTitle(int id, String title) {
		String query = "UPDATE Books SET title = ? WHERE bookID = ?;";
		
		try {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager.getConnection(DBConnection);
	      
	      preparedStatement = connect.prepareStatement(query);
	      preparedStatement.setString(1, title);
	      preparedStatement.setInt(2, id);
	      int flag = preparedStatement.executeUpdate();
	      if(flag == 0) {
	    	  throw new Exception();
	      }
	      
		} catch(SQLException s) {
	  		return false;
		} catch (Exception e) {
			return false;
		} finally {
	      close();
		}

		return true;
	}//end editBookTitle
	
		//returns true if edit successful, else false
	public boolean editBookGenre(int id, int genreID) {
		String query = "UPDATE Books SET genreID = ? WHERE bookID = ?;";
		
		try {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager.getConnection(DBConnection);
	      
	      preparedStatement = connect.prepareStatement(query);
	      preparedStatement.setInt(1, genreID);
	      preparedStatement.setInt(2, id);
	      int flag = preparedStatement.executeUpdate();
	      if(flag == 0) {
	    	  throw new Exception();
	      }
	      
		} catch(SQLException s) {
	  		return false;
		} catch (Exception e) {
			return false;
		} finally {
	      close();
		}

		return true;
	}//end editBookGenre
	
		//returns true if edit successful, else false
	public boolean editBookPublicationDate(int id, String date) {
		String query = "UPDATE Books SET publicationDate = ? WHERE bookID = ?;";
		
		try {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager.getConnection(DBConnection);
	      
	      preparedStatement = connect.prepareStatement(query);
	      preparedStatement.setString(1, date);
	      preparedStatement.setInt(2, id);
	      int flag = preparedStatement.executeUpdate();
	      if(flag == 0) {
	    	  throw new Exception();
	      }
	      
		} catch(SQLException s) {
	  		return false;
		} catch (Exception e) {
			return false;
		} finally {
	      close();
		}

		return true;
	}//end editBookPublicationDate
	
	
	/********************
	 *                  *
	 *  Rental Methods  *
	 *                  *
	 ********************
	 */
	
		//return true if check in successful
		//returns false if book is already checked into system and prints message
		//returns false if any errors occur
	public boolean checkInBookByRentalID(int rentalID, String date) {
		String query1 = "SELECT bookID FROM RentalHistory WHERE rentalID = ?;";
		String query2 = "UPDATE RentalHistory SET returnDate = ? WHERE rentalID = ?;";
		String query3 = "UPDATE Books SET inOutStatus = 0 WHERE bookID = ?;";
		String query4 = "SELECT Books.inOutStatus FROM Books, RentalHistory WHERE RentalHistory.rentalID = ? "
				      + "AND RentalHistory.bookID = Books.bookID;";
		
		int bookID = -1;
		
		try {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager.getConnection(DBConnection);
	      
	      preparedStatement = connect.prepareStatement(query4);
	      preparedStatement.setInt(1, rentalID);
	      resultSet = preparedStatement.executeQuery();
	      resultSet.next();
	      int isRented = resultSet.getInt("inOutStatus");
	      if(isRented == 0) {
	  	    	System.out.println("This book is already checked in, check that you have the correct Book ID.\n");
	  	    	return false;
	      }
	      
	      preparedStatement = connect.prepareStatement(query1);
	      preparedStatement.setInt(1, rentalID);
	      resultSet = preparedStatement.executeQuery();
      	  
	      if (resultSet.next() ) {
      		  bookID = resultSet.getInt("bookID");
      	  }
      	  else {
      		  throw new Exception("q1");
      	  }
	      
	      preparedStatement = connect.prepareStatement(query2);
	      preparedStatement.setString(1, date);
	      preparedStatement.setInt(2, rentalID);
	      int flag = preparedStatement.executeUpdate();
	      if(flag == 0) {
	    	  throw new Exception("q2");
	      }
      	  
      	  preparedStatement = connect.prepareStatement(query3);
	      preparedStatement.setInt(1, bookID);
	      int flag2 = preparedStatement.executeUpdate();
	      if(flag2 == 0) {
	    	  throw new Exception("q3");
	      }
	      
		} catch(SQLException s) {
			//System.out.println(s);
			return false;
		} catch (Exception e) {
			//System.out.println(e);
			return false;
		} finally {
			close();
		}

		return true;
	}//end checkInBookByRentalID
	
		//return true if check in successful
		//returns false if book is already checked into system and prints message
		//returns false if any errors occur
	public boolean checkInBookByBookID(int bookID, String date) {
		String query1 = "Select RentalHistory.rentalID FROM Books, RentalHistory WHERE Books.bookID = ? "
				      + "AND RentalHistory.bookID = Books.bookID;";
		String query2 = "UPDATE RentalHistory SET returnDate = ? WHERE rentalID = ?;";
		String query3 = "UPDATE Books SET inOutStatus = 0 WHERE bookID = ?;";
		String query4 = "SELECT Books.inOutStatus FROM Books, RentalHistory WHERE Books.bookID = ? "
			      	  + "AND RentalHistory.bookID = Books.bookID;";
		int rentalID = -1;
		
		try {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager.getConnection(DBConnection);
	      
	      preparedStatement = connect.prepareStatement(query4);
	      preparedStatement.setInt(1, bookID);
	      resultSet = preparedStatement.executeQuery();
	      resultSet.next();
	      int isRented = resultSet.getInt("inOutStatus");
	      if(isRented == 0) {
	  	    	System.out.println("This book is already checked in, check that you have the correct Book ID.\n");
	  	    	return false;
	      }
	      
	      preparedStatement = connect.prepareStatement(query1);
	      preparedStatement.setInt(1, bookID);
	      resultSet = preparedStatement.executeQuery();
      	  
      	  if (resultSet.next() ) {
      		  rentalID = resultSet.getInt("rentalID");
      	  }
      	  else {
      		  throw new Exception("q1");
      	  }
	      
	      preparedStatement = connect.prepareStatement(query2);
	      preparedStatement.setString(1, date);
	      preparedStatement.setInt(2, rentalID);
	      int flag = preparedStatement.executeUpdate();
	      if(flag == 0) {
	    	  throw new Exception("q2");
	      }
      	  
      	  preparedStatement = connect.prepareStatement(query3);
	      preparedStatement.setInt(1, bookID);
	      int flag2 = preparedStatement.executeUpdate();
	      if(flag2 == 0) {
	    	  throw new Exception("q3");
	      }
	      
		} catch(SQLException s) {
			//System.out.println(s);
			return false;
		} catch (Exception e) {
			//System.out.println(e);
			return false;
		} finally {
			close();
		}

		return true;
	}//end checkInBookByBookID
	
		//return true if check out successful
		//prints statement and returns false if member has 5 books already rented or if book is already checked out
		//returns false if operation otherwise cannot be completed
	public boolean checkOutBook(int memberID, int bookID, String rentalDate, String dueDate) {
		String query1 = "SELECT Members.memberID FROM Members, RentalHistory WHERE "
					  + "Members.memberID = ? AND RentalHistory.returnDate = NULL;";
		String query2 = "INSERT INTO RentalHistory (rentalID, memberID, bookID, rentalDate, dueDate, returnDate) "
				      + "VALUES (NULL, ?, ?, ?, ?, NULL);";
		String query3 = "UPDATE Books SET inOutStatus = 1 WHERE bookID = ?;";
		String query4 = "SELECT inOutStatus FROM Books WHERE bookID = ?;";
		
		try {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager.getConnection(DBConnection);
	      
	      preparedStatement = connect.prepareStatement(query4);
	      preparedStatement.setInt(1, bookID);
	      resultSet = preparedStatement.executeQuery();
	      resultSet.next();
	      int isRented = resultSet.getInt("inOutStatus");
	      if(isRented > 0) {
	  	    	System.out.println("This book is already checked out, check that you have the correct Book ID.\n");
	  	    	return false;
	      }

	      
	      preparedStatement = connect.prepareStatement(query1);
	      preparedStatement.setInt(1, memberID);
	      resultSet = preparedStatement.executeQuery();
	      if(resultSet.last()) {
	  	    int rentalCount = resultSet.getRow();
	  	    if(rentalCount >= 5) {
	  	    	System.out.println("Member already has 5 books rented.\n");
	  	    	return false;
	  	    }
	      }
	      
	      preparedStatement = connect.prepareStatement(query2);
	      preparedStatement.setInt(1, memberID);
	      preparedStatement.setInt(2, bookID);
	      preparedStatement.setString(3, rentalDate);
	      preparedStatement.setString(4, dueDate);
	      int flag = preparedStatement.executeUpdate();
	      if(flag == 0) {
	  	    throw new Exception("q2 insert");
	      }
		  
		  preparedStatement = connect.prepareStatement(query3);
	      preparedStatement.setInt(1, bookID);
	      int flag2 = preparedStatement.executeUpdate();
	      if(flag2 == 0) {
	  	    throw new Exception("q3 book toggle");
	      }
	    
		} catch(SQLException s) {
			//System.out.println(s);
			return false;
		} catch (Exception e) {
			//System.out.println(e);
			return false;
		} finally {
			close();
		}
	
		return true;
	}//end checkOutBook
	
	public String viewRentalsByRentalID(int rentalID) {
		String result = "";
		String query = "SELECT Books.title, Books.bookID, RentalHistory.rentalDate, RentalHistory.dueDate, RentalHistory.returnDate, "
				     + "Members.memberID, Members.fName, Members.lName "
				     + "FROM Books, RentalHistory, Members "
					 + "WHERE RentalHistory.rentalID = ? AND Members.memberID = RentalHistory.memberID "
					 + "AND RentalHistory.bookID = Books.bookID;";
		try {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager.getConnection(DBConnection);
	      
	      preparedStatement = connect.prepareStatement(query);
	      preparedStatement.setInt(1, rentalID);
	      resultSet = preparedStatement.executeQuery();
	  
	      while(resultSet.next()) {
	    	  result += "Title:   " + resultSet.getString("Books.title") 
	    			+ "\nID:      " + resultSet.getInt("Books.bookID")
	    			+ "\nRented: " + resultSet.getString("RentalHistory.rentalDate")
	    			+ "  Due Back: " + resultSet.getString("RentalHistory.dueDate");
	    	  if(resultSet.getString("RentalHistory.returnDate") != null) {
	    		  result += " Returned: " + resultSet.getString("RentalHistory.returnDate");
	    	  }
	    	  result +=  "\nMember: " + resultSet.getString("Members.memberID") + "   " 
	    	         + resultSet.getString("Members.fName") + " " + resultSet.getString("Members.lName") + "\n\n";
	      }
	      
		} catch(SQLException s) {
			System.out.println(s);
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			close();
		}
		
		return result;
	}//end viewRentalsByMemberID
	
	public String viewRentalsByMemberID(int memberID) {
		String result = "";
		String query = "SELECT Books.title, Books.bookID, RentalHistory.rentalDate, RentalHistory.dueDate, RentalHistory.returnDate, RentalHistory.rentalID "
				     + "FROM Books, RentalHistory, Members "
					 + "WHERE Members.memberID = ? AND Members.memberID = RentalHistory.memberID "
					 + "AND RentalHistory.bookID = Books.bookID AND RentalHistory.returnDate IS NULL ORDER BY Books.title;";
		try {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager.getConnection(DBConnection);
	      
	      preparedStatement = connect.prepareStatement(query);
	      preparedStatement.setInt(1, memberID);
	      resultSet = preparedStatement.executeQuery();
	  
	      while(resultSet.next()) {
	    	  result += "Rental ID:  " + resultSet.getString("RentalHistory.rentalID")
	    	  		 + "\nTitle:   " + resultSet.getString("Books.title") 
  			         + "\nID:      " + resultSet.getInt("Books.bookID")
  			         + "\nRented: " + resultSet.getString("RentalHistory.rentalDate")
  			         + "  Due Back: " + resultSet.getString("RentalHistory.dueDate");
	    	  if(resultSet.getString("RentalHistory.returnDate") != null) {
	    		  result += " Returned: " + resultSet.getString("RentalHistory.returnDate");
	    	  }
	    	  result +=  "\n\n";
	      }
	      
		} catch(SQLException s) {
			System.out.println(s);
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			close();
		}
		
		return result;
	}//end viewRentalsByMemberID
	
	public String viewAllIncompleteRentals() {
		String result = "";
		String query = "SELECT RentalHistory.rentalID, Members.MemberID, Books.title, Books.bookID FROM Books, RentalHistory, Members "
					 + "WHERE Members.memberID = RentalHistory.memberID "
					 + "AND RentalHistory.bookID = Books.bookID "
					 + "AND RentalHistory.returnDate IS NULL ORDER BY RentalHistory.rentalID;";
		try {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager.getConnection(DBConnection);
	      
	      preparedStatement = connect.prepareStatement(query);
	      resultSet = preparedStatement.executeQuery();
	  
	      while(resultSet.next()) {
	    	  result += "Rental ID:  " + resultSet.getInt("RentalHistory.rentalID") 
	    	  		  + "\nMember ID:  " + resultSet.getInt("RentalHistory.memberID")
	    	  		  + "\nTitle:   " + resultSet.getString("Books.title") 
	    			  + "\nID:      " + resultSet.getInt("Books.bookID")
	    			  + "\n\n";
	      }
	      
		} catch(SQLException s) {
			System.out.println(s);
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			close();
		}
		
		return result;
	}//end viewOverdueRentals
	
	
	/********************
	 *                  *
	 *  Member Methods  *
	 *                  *
	 ********************
	 */
	
		//returns true if member created successfully, else false
	public boolean createMember(String fName, String mName, String lName, String address, int phone) {
		String query = "INSERT INTO Members (memberID, fName, mName, lName, address, phone, balance) "
				 + "VALUES (NULL, ?, ?, ?, ?, ?, 0);";
	
		try {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager.getConnection(DBConnection);
	      
	      //prepare and run query1 : create book
	      preparedStatement = connect.prepareStatement(query);
	      preparedStatement.setString(1, fName);
	      preparedStatement.setString(2, mName);
	      preparedStatement.setString(3, lName);
	      preparedStatement.setString(4, address);
	      preparedStatement.setInt(5, phone);
	      int flag = preparedStatement.executeUpdate();
		      if(flag == 0) {
		    	  throw new Exception();
		      }
	      
		} catch(SQLException s) {
	  		return false;
		} catch (Exception e) {
			return false;
		} finally {
			close();
		}
		
		return true;
	}//end createMember
	
	
	/*
	 *  Removed delete book functionality for now.  We do not feel comfortable yet cascading changes
	 *  and restricting changes makes this method not useful.  For now we have decided this kind of change should
	 *  be completed by a SysAdmin on the database directly. 
	 *
		//returns true if delete successful
	public boolean deleteMember(int id) {
		String query = "DELETE FROM Members WHERE memberID = ?;";
		
		try {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager.getConnection(DBConnection);
	      
	      preparedStatement = connect.prepareStatement(query);
	      preparedStatement.setInt(1, id);
	      int flag = preparedStatement.executeUpdate();
	      if(flag == 0) {
	    	  throw new Exception();
	      }
	      
		} catch(SQLException s) {
	  		return false;
		} catch (Exception e) {
			return false;
		} finally {
			close();
		}

		return true;
	}//end deleteMember
	*
	*/
	
		//returns true if edit successful
	public boolean editMemberFName(int id, String fName) {
		String query = "UPDATE Members SET fName = ? WHERE memberID = ?;";
		
		try {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager.getConnection(DBConnection);
	      
	      preparedStatement = connect.prepareStatement(query);
	      preparedStatement.setString(1, fName);
	      preparedStatement.setInt(2, id);
	      int flag = preparedStatement.executeUpdate();
	      if(flag == 0) {
	    	  throw new Exception();
	      }
	      
		} catch(SQLException s) {
	  		return false;
		} catch (Exception e) {
			return false;
		} finally {
			close();
		}

		return true;
	}//end editMemberFName
	
	//returns true if edit successful
	public boolean editMemberMName(int id, String mName) {
		String query = "UPDATE Members SET mName = ? WHERE memberID = ?;";
		
		try {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager.getConnection(DBConnection);
	      
	      preparedStatement = connect.prepareStatement(query);
	      preparedStatement.setString(1, mName);
	      preparedStatement.setInt(2, id);
	      int flag = preparedStatement.executeUpdate();
	      if(flag == 0) {
	    	  throw new Exception();
	      }
	      
		} catch(SQLException s) {
	  		return false;
		} catch (Exception e) {
			return false;
		} finally {
			close();
		}

		return true;
	}//end editMemberLName
	
		//returns true if edit successful
	public boolean editMemberLName(int id, String lName) {
		String query = "UPDATE Members SET lName = ? WHERE memberID = ?;";
		
		try {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager.getConnection(DBConnection);
	      
	      preparedStatement = connect.prepareStatement(query);
	      preparedStatement.setString(1, lName);
	      preparedStatement.setInt(2, id);
	      int flag = preparedStatement.executeUpdate();
	      if(flag == 0) {
	    	  throw new Exception();
	      }
	      
		} catch(SQLException s) {
	  		return false;
		} catch (Exception e) {
			return false;
		} finally {
			close();
		}

		return true;
	}//end editMemberLName
	
		//return true if edit successful
	public boolean editMemberAddress(int id, String address) {
		String query = "UPDATE Members SET address = ? WHERE memberID = ?;";
		
		try {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager.getConnection(DBConnection);
	      
	      preparedStatement = connect.prepareStatement(query);
	      preparedStatement.setString(1, address);
	      preparedStatement.setInt(2, id);
	      int flag = preparedStatement.executeUpdate();
	      if(flag == 0) {
	    	  throw new Exception();
	      }
	      
		} catch(SQLException s) {
	  		return false;
		} catch (Exception e) {
			return false;
		} finally {
			close();
		}

		return true;
	}//end editMemberAddress
	
	//returns true if edit successful
	public boolean editMemberPhone(int id, int phone) {
		String query = "UPDATE Members SET phone = ? WHERE memberID = ?;";
		
		try {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager.getConnection(DBConnection);
	      
	      preparedStatement = connect.prepareStatement(query);
	      preparedStatement.setInt(1, phone);
	      preparedStatement.setInt(2, id);
	      int flag = preparedStatement.executeUpdate();
	      if(flag == 0) {
	    	  throw new Exception();
	      }
	      
		} catch(SQLException s) {
	  		return false;
		} catch (Exception e) {
			return false;
		} finally {
			close();
		}

		return true;
	}//end editMemberLName
	
		//return true if edit successful
	public boolean setMemberBalance(int id, float balance) {
		String query = "UPDATE Members SET balance = ? WHERE memberID = ?;";
		
		try {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager.getConnection(DBConnection);
	      
	      preparedStatement = connect.prepareStatement(query);
	      preparedStatement.setFloat(1, balance);
	      preparedStatement.setInt(2, id);
	      int flag = preparedStatement.executeUpdate();
	      if(flag == 0) {
	    	  throw new Exception();
	      }
	      
		} catch(SQLException s) {
	  		return false;
		} catch (Exception e) {
			return false;
		} finally {
			close();
		}

		return true;
	}//end setMemberBalance
	
	public String viewMemberByID(int id) {
		String result = "";
		String query = "SELECT * FROM Members WHERE memberID = ?;";
		
		try {
            // This will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.jdbc.Driver");
            // Setup the connection with the DB
            connect = DriverManager.getConnection(DBConnection);
            
            preparedStatement = connect.prepareStatement(query);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
            	result += "ID:   " + resultSet.getInt("memberID") 
            			+ "  Name: " + resultSet.getString("fName") + " ";
            	
            	if(resultSet.getString("mName") != null) {
            		result += resultSet.getString("mName") + " ";
            	}
            	
            	result +=  resultSet.getString("lName") 
            			+ "\nAddress: " + resultSet.getString("address") 
            			+ "  Phone: " + resultSet.getInt("phone") 
            			+ "  Balance: $" + resultSet.getFloat("balance") + "\n\n";
            }
        } catch(SQLException s) {
        	
        } catch (Exception e) {
            
        } finally {
            close();
        }
		
		return result;
	}//end viewMemberByID
	
	public String viewMemberByLastName(String lName) {
		String result = "";
		String query = "SELECT * FROM Members WHERE lName LIKE ? ORDER BY lName;";
		
		try {
            // This will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.jdbc.Driver");
            // Setup the connection with the DB
            connect = DriverManager.getConnection(DBConnection);
            
            preparedStatement = connect.prepareStatement(query);
            preparedStatement.setString(1, "%" + lName + "%");
            resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
            	result += "ID:   " + resultSet.getInt("memberID") 
    				    + "  Name: " + resultSet.getString("fName") + " ";
    	
            	if(resultSet.getString("mName") != null) {
            		result += resultSet.getString("mName") + " ";
            	}
    	
            	result +=  resultSet.getString("lName") 
            			+ "\nAddress: " + resultSet.getString("address") 
            			+ "  Phone: " + resultSet.getInt("phone") 
            			+ "  Balance: $" + resultSet.getFloat("balance") + "\n\n";
            }
        } catch(SQLException s) {
        	
        } catch (Exception e) {
            
        } finally {
            close();
        }
		
		return result;
	}//end viewMemberByName
	
	public String viewMembersWithBalance() {
		String result = "";
		String query = "SELECT * FROM Members WHERE balance > 0.00  ORDER BY lName;";
		
		try {
            // This will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.jdbc.Driver");
            // Setup the connection with the DB
            connect = DriverManager.getConnection(DBConnection);
            
            preparedStatement = connect.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
            	result += "ID:   " + resultSet.getInt("memberID") 
    				    + "  Name: " + resultSet.getString("fName") + " ";
    	
            	if(resultSet.getString("mName") != null) {
            		result += resultSet.getString("mName") + " ";
            	}
    	
            	result +=  resultSet.getString("lName") 
            			+ "\nAddress: " + resultSet.getString("address") 
            			+ "  Phone: " + resultSet.getInt("phone")  
            			+ "  Balance: $" + resultSet.getFloat("balance") + "\n\n";
            }
        } catch(SQLException s) {
        	
        } catch (Exception e) {
            
        } finally {
            close();
        }
		
		return result;
	}//end viewMembersWithBalance
	
	/********************
	 *                  *
	 *  Author Methods  *
	 *                  *
	 ********************
	 */
	
		//return true if add successful
	public boolean addAuthor(String fName, String mName, String lName) {
		String query = "INSERT INTO Authors (authorID, fName, mName, lName) "
				     + "VALUES (NULL, ?, ?, ?);";
	
		try {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager.getConnection(DBConnection);
	      
	      //prepare and run query1 : create book
	      preparedStatement = connect.prepareStatement(query);
	      preparedStatement.setString(1, fName);
	      preparedStatement.setString(2, mName);
	      preparedStatement.setString(3, lName);
	      int flag = preparedStatement.executeUpdate();
		      if(flag == 0) {
		    	  throw new Exception();
		      }
	      
		} catch(SQLException s) {
	  		return false;
		} catch (Exception e) {
			return false;
		} finally {
			close();
		}
		
		return true;
	}//end addAuthor
	
		//return true if edit successful
	public boolean editAuthorFName(int id, String fName) {
		String query = "UPDATE Authors SET fName = ? WHERE authorID = ?;";
		
		try {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager.getConnection(DBConnection);
	      
	      preparedStatement = connect.prepareStatement(query);
	      preparedStatement.setString(1, fName);
	      preparedStatement.setInt(2, id);
	      int flag = preparedStatement.executeUpdate();
	      if(flag == 0) {
	    	  throw new Exception();
	      }
	      
		} catch(SQLException s) {
	  		return false;
		} catch (Exception e) {
			return false;
		} finally {
			close();
		}

		return true;
	}//end editAuthorFName
	
		//return true if edit successful
	public boolean editAuthorMName(int id, String mName) {
		String query = "UPDATE Authors SET mName = ? WHERE authorID = ?;";
		
		try {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager.getConnection(DBConnection);
	      
	      preparedStatement = connect.prepareStatement(query);
	      preparedStatement.setString(1, mName);
	      preparedStatement.setInt(2, id);
	      int flag = preparedStatement.executeUpdate();
	      if(flag == 0) {
	    	  throw new Exception();
	      }
	      
		} catch(SQLException s) {
	  		return false;
		} catch (Exception e) {
			return false;
		} finally {
			close();
		}

		return true;
	}//end editAuthorMName
	
		//return true if edit successful
	public boolean editAuthorLName(int id, String lName) {
		String query = "UPDATE Authors SET lName = ? WHERE authorID = ?;";
		
		try {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager.getConnection(DBConnection);
	      
	      preparedStatement = connect.prepareStatement(query);
	      preparedStatement.setString(1, lName);
	      preparedStatement.setInt(2, id);
	      int flag = preparedStatement.executeUpdate();
	      if(flag == 0) {
	    	  throw new Exception();
	      }
	      
		} catch(SQLException s) {
	  		return false;
		} catch (Exception e) {
			return false;
		} finally {
			close();
		}

		return true;
	}//end editAuthorLName
	
	public boolean connectAuthorToISBN(int authorID, int ISBN) {
		String query = "INSERT INTO AuthorToISBN (authorID, ISBN) VALUES (?, ?);";
		
		try {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager.getConnection(DBConnection);
	      
	      preparedStatement = connect.prepareStatement(query);
	      preparedStatement.setInt(1, authorID);
	      preparedStatement.setInt(2, ISBN);
	      int flag = preparedStatement.executeUpdate();
	      if(flag == 0) {
	    	  throw new Exception();
	      }
	      
		} catch(SQLException s) {
	  		return false;
		} catch (Exception e) {
			return false;
		} finally {
			close();
		}

		return true;
	}//end connectAuthorToISBN
	
	public String viewAuthorInfoByID(int id) {
		String result = "";
		String query = "SELECT * FROM Authors WHERE authorID = ?;";
		
		try {
            // This will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.jdbc.Driver");
            // Setup the connection with the DB
            connect = DriverManager.getConnection(DBConnection);
            
            preparedStatement = connect.prepareStatement(query);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
            	result += "ID:   " + resultSet.getInt("authorID") 
            			+ "  Name: " + resultSet.getString("fName") + " ";
            			
            	if(resultSet.getString("mName") != null) {
            		result += resultSet.getString("mName") + " ";
            	}
            	
            	result += resultSet.getString("lName")  + "\n\n";
            }
        } catch(SQLException s) {
        	
        } catch (Exception e) {
            
        } finally {
            close();
        }
		
		return result;
	}//end viewAuthorInfoByID
	
	public String viewAuthorInfoByLastName(String lName) {
		String result = "";
		String query = "SELECT * FROM Authors WHERE lName LIKE ?  ORDER BY lName;";
		
		try {
            // This will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.jdbc.Driver");
            // Setup the connection with the DB
            connect = DriverManager.getConnection(DBConnection);
            
            preparedStatement = connect.prepareStatement(query);
            preparedStatement.setString(1, "%" + lName + "%");
            resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
            	result += "ID:   " + resultSet.getInt("authorID") 
            			+ "  Name: " + resultSet.getString("fName") + " ";
            			
            	if(resultSet.getString("mName") != null) {
            		result += resultSet.getString("mName") + " ";
            	}
            	
            	result += resultSet.getString("lName")  + "\n\n";
            }
        } catch(SQLException s) {
        	
        } catch (Exception e) {
            
        } finally {
            close();
        }
		
		return result;
	}//end viewAuthorInfoByLastName
	
	public String viewAllAuthors() {
		String result = "";
		String query = "SELECT * FROM Authors ORDER BY lName;";
		
		try {
            // This will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.jdbc.Driver");
            // Setup the connection with the DB
            connect = DriverManager.getConnection(DBConnection);
            
            preparedStatement = connect.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()) {
            	result += "ID:   " + resultSet.getInt("authorID") 
            			+ "  Name: " + resultSet.getString("fName") + " ";
            			
            	if(resultSet.getString("mName") != null) {
            		result += resultSet.getString("mName") + " ";
            	}
            	
            	result += resultSet.getString("lName")  + "\n\n";
            }
        } catch(SQLException s) {
        	
        } catch (Exception e) {
            
        } finally {
            close();
        }
		
		return result;
	}//end viewAllAuthors
	
	/*******************
	 *                 *
	 *  Genre Methods  *
	 *                 *
	 *******************
	 */
	
	public String viewGenreList() {
		String result = "";
		String query = "SELECT * FROM Genres;";
		
		try {
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager.getConnection(DBConnection);
	      
	      statement = connect.createStatement();
	      resultSet = statement.executeQuery(query);
	  
	      while(resultSet.next()) {
	    	  result += "ID: " + resultSet.getInt("genreID") 
	    	  		  + " -- " + resultSet.getString("genreName")
	    			  + "\n";
	      }
	      
		} catch(SQLException s) {
			
		} catch (Exception e) {
			
		} finally {
			close();
		}
		
		return result;
	}//end viewGenreList
	
	
	/*******************
	 *                 *
	 *  Other Methods  *
	 *                 *
	 *******************
	 */
	
		// You need to close the resultSet
    private void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {

        }
    }//end close
	
}//end class
