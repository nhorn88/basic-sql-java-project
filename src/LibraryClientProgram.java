
/**********************************************
 * CS 485: Database Sys Design
 * Project 1: Library Database
 * Authors: Nick Horn & William Newshutz
 * 
 * File: LibraryClientProgram.java
 * Desc: Runs Library app through console
 * 		 based on user-inputs to access and 
 *       manipulate the Library database.
 *********************************************
 */

import java.util.Scanner;

public class LibraryClientProgram {

	public static void main(String[] args) {
		Scanner console = new Scanner(System.in);
		Librarian lib = new Librarian();
		String password = "";
		boolean loggedIn = false;
		String inputString = "";
		String resultString = "";
		boolean resultFlag = true;
		boolean bookFlag, bookSearchFlag, bookEditFlag;
		boolean memberFlag, memberSearchFlag, memberEditFlag;
		boolean rentalFlag, rentalSearchFlag, rentalCheckInFlag;
		boolean authorFlag, authorSearchFlag, authorEditFlag;
		int input = -1;
		
		int bookID, ISBN, authorID, genreID, memberID, rentalID, phone;
		String title, pubDate, fName, mName, lName, address, rentalDate, dueDate, returnDate;
		float balance;
		
		System.out.println("\n-------------------------------------\n"
				           + "|      Library Records System       |\n"
				           + "|                                   |\n"
				           + "|  For all menu selections:         |\n"
				           + "|  Enter the number corresponding   |\n"
				           + "|  to the option you are selecting. |\n"
				           + "-------------------------------------\n");
		
		//lib.connectionTest();
		
		System.out.print("Enter Password:");
		
		try {
			password = console.next();
			loggedIn = lib.validatePassword(password);
		}
		catch(Exception e) {
			
		}
		finally {
			
		}
		
		if(loggedIn) {
			System.out.println("\nAccess Granted\n");
		} else {
			System.out.println("\nAccess Denied\n");
		}
		
		
		//main loop runs infinitely till user selects to end program via mainMenu
		//terminates via System.exit(0)
		while(loggedIn) {
			try {
				mainMenu();
				input = console.nextInt();
				
				switch(input) {
					
					case 0: //end program
						System.out.println("\n----->Ending Program\n");
						console.close();
						System.exit(0);
						
					case 1: //goto bookMenu
						bookFlag = true;
						while(bookFlag) {
							try{
								bookMenu();
								input = console.nextInt();
								
								switch(input) {
									case 0: //return to previous menu
										bookFlag = false;
										break;
										
									case 1: //goto bookSeachMenu
										bookSearchFlag = true;
										while(bookSearchFlag) {
											try {
												bookSearchMenu();
												input = console.nextInt();
												
												switch(input) {
													case 0: //return to previous menu
														bookSearchFlag = false;
														break;
														
													case 1: //book ID
														System.out.print("Enter Book ID: ");
														input = console.nextInt();
														resultString = lib.searchBooksByID(input);
														if(resultString != null && resultString.length() != 0) {
															System.out.println("\n" + resultString);
														}
														else {
															System.out.println("\nNo results found.\n");
														}
														
														break;
														
													case 2: //ISBN
														System.out.print("Enter Book ISBN: ");
														input = console.nextInt();
														resultString = lib.searchBooksByISBN(input);
														if(resultString != null && resultString.length() != 0) {
															System.out.println("\n" + resultString);
														}
														else {
															System.out.println("\nNo results found.\n");
														}
														
														break;
														
													case 3: //title
														System.out.print("Enter Book Title: (Accepts Partial Titles) ");
														console.nextLine();
														inputString = console.nextLine();
														resultString = lib.searchBooksByTitle(inputString);
														if(resultString != null && resultString.length() != 0) {
															System.out.println("\n" + resultString);
														}
														else {
															System.out.println("\nNo results found.\n");
														}
														
														break;
														
													case 4: //genre
														System.out.println(lib.viewGenreList());
														System.out.print("Enter Book Genre ID: ");
														input = console.nextInt();
														resultString = lib.searchBooksByGenreID(input);
														if(resultString != null && resultString.length() != 0) {
															System.out.println("\n" + resultString);
														}
														else {
															System.out.println("\nNo results found.\n");
														}
														
														break;
														
													case 5: //author ID
														System.out.print("Enter Author ID: ");
														input = console.nextInt();
														resultString = lib.searchBooksByAuthorID(input);
														if(resultString != null && resultString.length() != 0) {
															System.out.println("\n" + resultString);
														}
														else {
															System.out.println("\nNo results found.\n");
														}
														
														break;
														
													case 6: //author last name
														System.out.print("Enter Author Last Name: (Accepts Partial Names) ");
														console.nextLine();
														inputString = console.nextLine();
														resultString = lib.searchBooksByAuthorLName(inputString);
														if(resultString != null && resultString.length() != 0) {
															System.out.println("\n" + resultString);
														}
														else {
															System.out.println("\nNo results found.\n");
														}
														
														break;
														
													case 7: //publication date
														System.out.print("Enter Book Publication Date: (Accepts Partial Dates) ");
														console.nextLine();
														inputString = console.nextLine();
														resultString = lib.searchBooksByReleaseDate(inputString);
														if(resultString != null && resultString.length() != 0) {
															System.out.println("\n" + resultString);
														}
														else {
															System.out.println("\nNo results found.\n");
														}
														
														break;
														
													default: //invalid input
														throw new Exception();
														
												}//end bookSearchMenu switch
											}
											catch(Exception e) {
												System.out.println("\n!!!! Invalid Selection: Please try again. !!!!");
												console.next();
												continue;
											}//end try-catch, bookSearchMenu
										}//end bookSearchMenu loop
										
										break;
									
									case 2: //add new book
										System.out.print("\nEnter ISBN: ");
										ISBN = console.nextInt();
										
										System.out.print("\nEnter Title: ");
										console.nextLine();
										title = console.nextLine();
										
										System.out.print("\nEnter Genre ID: ");
										genreID = console.nextInt();
										
										System.out.print("\nEnter Author ID: ");
										authorID = console.nextInt();
										
										System.out.print("\nEnter Publication Date: ");
										console.nextLine();
										pubDate = console.nextLine();
										
										resultFlag = lib.addNewBook(ISBN, title, genreID, authorID, pubDate);
										
										if(resultFlag) {
											System.out.println("\nBook Successfully Added.\n");
										}
										else {
											System.out.println("\nBook failed to add, check that your ID's are valid.\n");
										}
										
										break;
										
									case 3: //goto bookEditMenu
										bookEditFlag = true;
										while(bookEditFlag) {
											try {
												bookEditMenu();
												input = console.nextInt();
												
												switch(input) {
													case 0: //return to previous menu
														bookEditFlag = false;
														break;
														
													case 1: //ISBN
														System.out.print("Enter ID of book to edit: ");
														bookID = console.nextInt();
														System.out.print("Enter new ISBN: ");
														ISBN = console.nextInt();
														resultFlag = lib.editBookISBN(bookID, ISBN);
														if(resultFlag) {
															System.out.println("\nBook #" + bookID + " updated successfully.\n");
														}
														else {
															System.out.println("\nEdit failed, check that your ID is valid.\n");
														}
														
														break;
														
													case 2: //title
														System.out.print("Enter ID of book to edit: ");
														bookID = console.nextInt();
														System.out.print("Enter new title: ");
														console.nextLine();
														title = console.nextLine();
														resultFlag = lib.editBookTitle(bookID, title);
														if(resultFlag) {
															System.out.println("\nBook #" + bookID + " updated successfully.\n");
														}
														else {
															System.out.println("\nEdit failed, check that your ID is valid.\n");
														}
														
														break;
														
													case 3: //genre ID
														System.out.println(lib.viewGenreList());
														System.out.print("Enter ID of book to edit: ");
														bookID = console.nextInt();
														System.out.print("Enter genre ID to give this book: ");
														genreID = console.nextInt();
														resultFlag = lib.editBookGenre(bookID, genreID);
														if(resultFlag) {
															System.out.println("\nBook #" + bookID + " updated successfully.\n");
														}
														else {
															System.out.println("\nEdit failed, check that your ID's are valid.\n");
														}
														
														break;
														
													case 4: //publication date
														System.out.print("Enter ID of book to edit: ");
														bookID = console.nextInt();
														System.out.print("Enter new publication date: (mm-dd-yyyy format preferred) ");
														console.nextLine();
														pubDate = console.nextLine();
														resultFlag = lib.editBookPublicationDate(bookID, pubDate);
														if(resultFlag) {
															System.out.println("\nBook #" + bookID + " updated successfully.\n");
														}
														else {
															System.out.println("\nEdit failed, check that your ID is valid.\n");
														}
														
														break;
														
													default: //invalid input
														throw new Exception();
														
												}//end bookEditMenu switch
											}
											catch(Exception e) {
												System.out.println("\n!!!! Invalid Selection: Please try again. !!!!");
												console.next();
												continue;
											}//end try-catch bookEditMenu
										}//end bookEditMenu loop
										
										break;	
										
									default: //invalid input
										throw new Exception();
										
								}//end bookMenu switch

							}
							catch(Exception e) {
								System.out.println("\n!!!! Invalid Selection: Please try again. !!!!");
								console.next();
								continue;
							}//end try-catch, bookMenu
						}//end bookMenu loop
						
						break;
					
					case 2: //goto memberMenu
						memberFlag = true;
						while(memberFlag) {
							try{
								memberMenu();
								input = console.nextInt();
								
								switch(input) {
									case 0: //return to previous menu
										memberFlag = false;
										break;
										
									case 1: //goto memberSearchMenu
										memberSearchFlag = true;
										while(memberSearchFlag) {
											try {
												memberSearchMenu();
												input = console.nextInt();
												
												switch(input) {
													case 0: //return to previous menu
														memberSearchFlag = false;
														break;
														
													case 1: //ID
														System.out.print("Enter Member ID: ");
														input = console.nextInt();
														resultString = lib.viewMemberByID(input);
														if(resultString != null && resultString.length() != 0) {
															System.out.println("\n" + resultString);
														}
														else {
															System.out.println("\nNo results found.\n");
														}
														
														break;
														
													case 2: //last name
														System.out.print("Enter Member Last Name: (Accepts Partial Names) ");
														console.nextLine();
														inputString = console.nextLine();
														resultString = lib.viewMemberByLastName(inputString);
														if(resultString != null && resultString.length() != 0) {
															System.out.println("\n" + resultString);
														}
														else {
															System.out.println("\nNo results found.\n");
														}
														
														break;
														
													default: //invalid input
														throw new Exception();
												}//end memberSearchMenu switch
											}
											catch(Exception e) {
												System.out.println("\n!!!! Invalid Selection: Please try again. !!!!");
												console.next();
												continue;
											}//end try-catch, memberSearchMenu
										}//end memberSearchMenu loop
										
										break;
									
									case 2: //view rentals by member id
										System.out.print("Enter Member ID: ");
										input = console.nextInt();
										resultString = lib.viewRentalsByMemberID(input);
										
										if(resultString != null && resultString.length() != 0) {
											System.out.println("\n" + resultString);
										}
										else {
											System.out.println("\nNo results found.\n");
										}
										
										break;
										
									case 3: //view all members with balance > 0.00
										resultString = lib.viewMembersWithBalance();
										
										if(resultString != null && resultString.length() != 0) {
											System.out.println("\n" + resultString);
										}
										else {
											System.out.println("\nNo results found.\n");
										}
										
										break;	
										
									case 4: //add new member
										System.out.print("Enter Member First Name: ");
										console.nextLine();
										fName = console.nextLine();
										
										System.out.print("Enter Member Middle Name: ");
										console.nextLine();
										mName = console.nextLine();
										
										if(mName.length() == 0 || mName.equals(" ")) {
											mName = null;
										}
										
										System.out.print("Enter Member Last Name: ");
										//console.nextLine();
										lName = console.nextLine();
										
										System.out.print("Enter Member Address: ");
										//console.nextLine();
										address = console.nextLine();
										
										System.out.print("Enter Member Phone Number (no dashes, spaces or parenthesis): ");
										phone = console.nextInt();
										
										resultFlag = lib.createMember(fName, lName, mName, address, phone);
										
										if(resultFlag) {
											System.out.println("\nMember Successfully Added.\n");
										}
										else {
											System.out.println("\nMember failed to add.\n");
										}
										
										break;
										
									case 5: //goto memberEditMenu
										memberEditFlag = true;
										while(memberEditFlag) {
											try {
												memberEditMenu();
												input = console.nextInt();
												
												switch(input) {
													case 0: //return to previous menu
														memberEditFlag = false;
														break;
														
													case 1: //first name
														System.out.print("Enter ID of member to update: ");
														memberID = console.nextInt();
														System.out.print("Enter new first name: ");
														console.nextLine();
														fName = console.nextLine();
														resultFlag = lib.editMemberFName(memberID, fName);
														if(resultFlag) {
															System.out.println("\nMember #" + memberID + " updated successfully.\n");
														}
														else {
															System.out.println("\nEdit failed, check that your ID is valid.\n");
														}
														
														break;
													
													case 2: //middle name
														System.out.print("Enter ID of member to update: ");
														memberID = console.nextInt();
														System.out.print("Enter new middle name: ");
														console.nextLine();
														mName = console.nextLine();
														resultFlag = lib.editMemberMName(memberID, mName);
														if(resultFlag) {
															System.out.println("\nMember #" + memberID + " updated successfully.\n");
														}
														else {
															System.out.println("\nEdit failed, check that your ID is valid.\n");
														}
														
														break;
														
													case 3: //last name
														System.out.print("Enter ID of member to update: ");
														memberID = console.nextInt();
														System.out.print("Enter new last name: ");
														console.nextLine();
														lName = console.nextLine();
														resultFlag = lib.editMemberLName(memberID, lName);
														if(resultFlag) {
															System.out.println("\nMember #" + memberID + " updated successfully.\n");
														}
														else {
															System.out.println("\nEdit failed, check that your ID is valid.\n");
														}
														
														break;
														
													case 4: //address
														System.out.print("Enter ID of member to update: ");
														memberID = console.nextInt();
														System.out.print("Enter new address: ");
														console.nextLine();
														address = console.nextLine();
														resultFlag = lib.editMemberAddress(memberID, address);
														if(resultFlag) {
															System.out.println("\nMember #" + memberID + " updated successfully.\n");
														}
														else {
															System.out.println("\nEdit failed, check that your ID is valid.\n");
														}
														
														break;
														
													case 5: //phone number
														System.out.print("Enter ID of member to update: ");
														memberID = console.nextInt();
														System.out.print("Enter new phone number (no dashes, spaces or parenthesis): ");
														phone = console.nextInt();
														resultFlag = lib.editMemberPhone(memberID, phone);
														if(resultFlag) {
															System.out.println("\nMember #" + memberID + " updated successfully.\n");
														}
														else {
															System.out.println("\nEdit failed, check that your ID is valid.\n");
														}
														
														break;
														
													case 6: //set balance
														System.out.print("Enter ID of member to update: ");
														memberID = console.nextInt();
														System.out.print("Enter new balance: ");
														balance = console.nextFloat();
														resultFlag = lib.setMemberBalance(memberID, balance);
														if(resultFlag) {
															System.out.println("\nMember #" + memberID + " updated successfully.\n");
														}
														else {
															System.out.println("\nEdit failed, check that your ID is valid.\n");
														}
														
														break;
														
													default: //invalid input
														throw new Exception();
														
												}//end memberEditMenu switch
											}
											catch(Exception e) {
												System.out.println("\n!!!! Invalid Selection: Please try again. !!!!");
												console.next();
												continue;
											}//end try-catch, memberEditMenu
										}//end memberEditMenu loop
										
										break;
										
									default: //invalid input
										throw new Exception();
									
								}//end memberMenu switch
							}
							catch(Exception e) {
								System.out.println("\n!!!! Invalid Selection: Please try again. !!!!");
								console.next();
								continue;
							}//end try-catch, memberMenu
						}//end memberMenu loop
						
						break;
						
					case 3: //goto rentalMenu
						rentalFlag = true;
						while(rentalFlag) {
							try{
								rentalMenu();
								input = console.nextInt();
								
								switch(input) {
									case 0: //return to previous menu
										rentalFlag = false;
										break;
										
									case 1: //goto rentalSearchMenu
										rentalSearchFlag = true;
										while(rentalSearchFlag) {
											try {
												rentalSearchMenu();
												input = console.nextInt();
												
												switch(input) {
													case 0: //return to previous menu
														rentalSearchFlag = false;
														break;
														
													case 1: //rental ID
														System.out.print("Enter Rental ID: ");
														rentalID = console.nextInt();
														
														resultString = lib.viewRentalsByRentalID(rentalID);
														if(resultString != null && resultString.length() != 0) {
															System.out.println("\n" + resultString);
														}
														else {
															System.out.println("\nNo results found.\n");
														}
														
														break;
														
													case 2: //member ID
														System.out.print("Enter Member ID: ");
														memberID = console.nextInt();
														
														resultString = lib.viewRentalsByMemberID(memberID);
														if(resultString != null && resultString.length() != 0) {
															System.out.println("\n" + resultString);
														}
														else {
															System.out.println("\nNo results found.\n");
														}
														
														break;
														
													case 3: //view all incomplete rentals
														resultString = lib.viewAllIncompleteRentals();
														if(resultString != null && resultString.length() != 0) {
															System.out.println("\n" + resultString);
														}
														else {
															System.out.println("\nNo results found.\n");
														}
														
														break;
														
													default: //invalid input
														throw new Exception();
														
												}//end rentalSearchMenu switch
											}
											catch(Exception e) {
												System.out.println("\n!!!! Invalid Selection: Please try again. !!!!");
												console.next();
												continue;
											}//end try-catch, rentalSearchMenu
										}//end rentalSearchMenu loop
										
										break;
									
									case 2: //goto rentalCheckInMenu
										rentalCheckInFlag = true;
										while(rentalCheckInFlag) {
											try {
												rentalCheckInMenu();
												input = console.nextInt();
												
												switch(input) {
													case 0: //return to previous menu
														rentalCheckInFlag = false;
														break;
														
													case 1: //rental ID
														System.out.print("Enter Rental ID: ");
														rentalID = console.nextInt();
														
														System.out.print("Enter Today's Date: (mm-dd-yyyy format preferred) ");
														console.nextLine();
														returnDate = console.nextLine();
														
														resultFlag = lib.checkInBookByRentalID(rentalID, returnDate);
														
														if(resultFlag) {
															System.out.println("\nBook checked in successfully.\n");
														}
														else {
															System.out.println("\nCheck in failed. If this is the only error messege: check that your ID is valid.\n");
														}
														
														break;
														
													case 2: //book ID
														System.out.print("Enter Book ID: ");
														bookID = console.nextInt();
														
														System.out.print("Enter Today's Date: (mm-dd-yyyy format preferred) ");
														console.nextLine();
														returnDate = console.nextLine();
														
														resultFlag = lib.checkInBookByBookID(bookID, returnDate);
														
														if(resultFlag) {
															System.out.println("\nBook checked in successfully.\n");
														}
														else {
															System.out.println("\nCheck in failed. If this is the only error messege: check that your ID is valid.\n");
														}
														
														break;
														
													default: //invalid input
														throw new Exception();
														
												}//end rentalCheckInMenu switch
											}
											catch(Exception e) {
												System.out.println("\n!!!! Invalid Selection: Please try again. !!!!");
												console.next();
												continue;
											}//end try-catch, rentalCheckInMenu
										}//end rentalCheckInMenu loop
										
										break;
										
									case 3: //check out book
										System.out.print("Enter Member ID: ");
										memberID = console.nextInt();
										
										System.out.print("Enter Book ID: ");
										bookID = console.nextInt();
										
										System.out.print("Enter Today's Date: (mm-dd-yyyy format preferred) ");
										console.nextLine();
										rentalDate = console.nextLine();
										
										System.out.print("Enter Return Due Date: (mm-dd-yyyy format preferred) ");
										//console.nextLine();
										dueDate = console.nextLine();
										
										resultFlag = lib.checkOutBook(memberID, bookID, rentalDate, dueDate);
										if(resultFlag) {
											System.out.println("Rental Successful.");
										}
										else {
											System.out.println("Rental Failed. If this is the only failure messege: check that your ID's are valid.");
										}
										
										break;	
										
									default: //invalid input
										throw new Exception();
									
								}//end rentalMenu switch
							}
							catch(Exception e) {
								System.out.println("\n!!!! Invalid Selection: Please try again. !!!!");
								console.next();
								continue;
							}//end try-catch, rentalMenu
						}//end rentalMenu loop
						
						break;
						
					case 4: //goto authorMenu
						authorFlag = true;
						while(authorFlag) {
							try{
								authorMenu();
								input = console.nextInt();
								
								switch(input) {
									case 0: //return to previous menu
										authorFlag = false;
										break;
										
									case 1: //goto authorSearchMenu
										authorSearchFlag = true;
										while(authorSearchFlag) {
											try {
												authorSearchMenu();
												input = console.nextInt();
												
												switch(input) {
													case 0: //return to previous menu
														authorSearchFlag = false;
														break;
														
													case 1: //ID
														System.out.print("Enter Author ID: ");
														input = console.nextInt();
														resultString = lib.viewAuthorInfoByID(input);
														if(resultString != null && resultString.length() != 0) {
															System.out.println("\n" + resultString);
														}
														else {
															System.out.println("\nNo results found.\n");
														}
														
														break;
														
													case 2: //last name
														System.out.print("Enter Author Last Name: (Accepts Partial Names) ");
														console.nextLine();
														inputString = console.nextLine();
														resultString = lib.viewAuthorInfoByLastName(inputString);
														if(resultString != null && resultString.length() != 0) {
															System.out.println("\n" + resultString);
														}
														else {
															System.out.println("\nNo results found.\n");
														}
														
														break;
														
													default: //invalid input
														throw new Exception();
														
												}//end authorSearchMenu switch
											}
											catch(Exception e) {
												System.out.println("\n!!!! Invalid Selection: Please try again. !!!!");
												console.next();
												continue;
											}//end try-catch, authorSearchMenu
										}//end authorSearchMenu loop
										
										break;
									
									case 2: //view all authors
										resultString = lib.viewAllAuthors();
										if(resultString != null && resultString.length() != 0) {
											System.out.println("\n" + resultString);
										}
										else {
											System.out.println("\nNo results found.\n");
										}
										
										break;
										
									case 3: //add new author
										System.out.print("Enter Author First Name: ");
										console.nextLine();
										fName = console.nextLine();
										
										System.out.print("Enter Author Middle Name: ");
										//console.nextLine();
										mName = console.nextLine();
										
										if(mName.length() == 0 || mName.equals(" ")) {
											mName = null;
										}
										
										System.out.print("Enter Author Last Name: ");
										//console.nextLine();
										lName = console.nextLine();
										
										resultFlag = lib.addAuthor(fName, mName, lName);
										if(resultFlag) {
											System.out.println("\nAuthor Successfully Added.\n");
										}
										else {
											System.out.println("\nAuthor failed to add.\n");
										}
										
										break;	
										
									case 4: //goto authorEditMenu
										authorEditFlag = true;
										while(authorEditFlag) {
											try {
												authorEditMenu();
												input = console.nextInt();
												
												switch(input) {
													case 0: //return to previous menu
														authorEditFlag = false;
														break;
														
													case 1: //first name
														System.out.print("Enter ID of author to update: ");
														authorID = console.nextInt();
														
														System.out.print("Enter new first name: ");
														console.nextLine();
														fName = console.nextLine();
														
														resultFlag = lib.editAuthorFName(authorID, fName);
														if(resultFlag) {
															System.out.println("\nAuthor #" + authorID + " updated successfully.\n");
														}
														else {
															System.out.println("\nEdit failed, check that your ID is valid.\n");
														}
														
														break;
														
													case 2: //middle name
														System.out.print("Enter ID of author to update: ");
														authorID = console.nextInt();
														
														System.out.print("Enter new middle name: ");
														console.nextLine();
														mName = console.nextLine();
														
														if(mName.length() == 0 || mName.equals(" ")) {
															mName = null;
														}
														
														resultFlag = lib.editAuthorMName(authorID, mName);
														if(resultFlag) {
															System.out.println("\nAuthor #" + authorID + " updated successfully.\n");
														}
														else {
															System.out.println("\nEdit failed, check that your ID is valid.\n");
														}
														
														break;
														
													case 3: //last name
														System.out.print("Enter ID of author to update: ");
														authorID = console.nextInt();
														
														System.out.print("Enter new last name: ");
														console.nextLine();
														lName = console.nextLine();
														
														resultFlag = lib.editAuthorLName(authorID, lName);
														if(resultFlag) {
															System.out.println("\nAuthor #" + authorID + " updated successfully.\n");
														}
														else {
															System.out.println("\nEdit failed, check that your ID is valid.\n");
														}
														
														break;
														
													default:
														throw new Exception();
														
												}//end authorEditMenu switch
											}
											catch(Exception e) {
												System.out.println("\n!!!! Invalid Selection: Please try again. !!!!");
												console.next();
												continue;
											}//end try-catch, authorEditMenu
										}//end authorEditMenu loop
										
										break;
										
									case 5: //add author to ISBN
										System.out.print("Enter Author ID: ");
										authorID = console.nextInt();
										
										System.out.print("Enter Book ISBN: ");
										ISBN = console.nextInt();
										
										resultFlag = lib.connectAuthorToISBN(authorID, ISBN);
										if(resultFlag) {
											System.out.println("\nAuthor Successfully Connected to ISBN.\n");
										}
										else {
											System.out.println("\nFailed to connect, connection may already exist.\n");
										}
										
										break;
										
									default: //invalid input
										throw new Exception();
									
								}//end authorMenu switch
							}
							catch(Exception e) {
								System.out.println("\n!!!! Invalid Selection: Please try again. !!!!");
								console.next();
								continue;
							}//end try-catch, authorMenu
						}//end authorMenu loop
						
						break;
						
					default: //input out of range
						throw new Exception();
						
				}//end mainMenu switch
				
			}
			catch(Exception e) {
				System.out.println("\n!!!! Invalid Selection: Please try again. !!!!");
				console.next();
				continue;
			}//end main try-catch, main menu
			
			
		}//end main loop System.exit(0) terminates
		
		
		
	}//end main

	public static void mainMenu() {
		System.out.println("\n----->Main Menu\n\n"
						 + "Make a selection:\n"
						 + "  0 : Exit Program\n" //end program
						 + "  1 : Books\n" //goto bookMenu
						 + "  2 : Members\n" //goto memberMenu
						 + "  3 : Rentals\n" //goto rentalMenu
						 + "  4 : Authors"); //goto authorMenu
		System.out.print("Enter selection: ");
	}//end mainMenu
	
	public static void bookMenu() {
		System.out.println("\n----->Book Menu\n\n"
						 + "Make a selection:\n"
						 + "  0 : Exit Book Menu\n" //goto mainMenu
						 + "  1 : Search Books\n" //goto bookSearchMenu
						 + "  2 : Add New Book to System\n"
						 + "  3 : Edit Existing Book"); //goto bookEditMenu
		System.out.print("Enter selection: ");
	}//end bookMenu
	
	public static void bookSearchMenu() {
		System.out.println("\n----->Book Search Menu\n\n"
						 + "Select book search criteria:\n"
						 + "  0 : Exit Book Search Menu\n" //goto bookMenu
						 + "  1 : Book ID\n"
						 + "  2 : Book ISBN\n"
						 + "  3 : Book Title\n"
						 + "  4 : Book Genre\n"
						 + "  5 : Author ID\n"
						 + "  6 : Author Last Name\n"
						 + "  7 : Book Publication Date");
		System.out.print("Enter selection: ");
	}//end bookSearchMenu
	
	public static void bookEditMenu() {
		System.out.println("\n----->Book Edit Menu\n\n"
						 + "Select book edit criteria:\n"
						 + "  0 : Exit Book Edit Menu\n" //goto bookMenu
						 + "  1 : Book ISBN\n"
						 + "  2 : Book Title\n"
						 + "  3 : Book Genre\n"
						 + "  4 : Book Publication Date");
		System.out.print("Enter selection: ");
	}//end bookEditMenu
	
	public static void memberMenu() {
		System.out.println("\n----->Member Menu\n\n"
						 + "Make a selection:\n"
						 + "  0 : Exit Member Menu\n" //goto mainMenu
						 + "  1 : Search Members\n" //goto memberSearchMenu
						 + "  2 : View Rentals by Member ID\n"
						 + "  3 : View All Members With Outstanding Balance\n"
						 + "  4 : Add New Member to System\n"
						 + "  5 : Update Existing Member"); //goto memberEditMenu
		System.out.print("Enter selection: ");
	}//end memberMenu
	
	public static void memberSearchMenu() {
		System.out.println("\n----->Member Search Menu\n\n"
						 + "Select member search criteria:\n"
						 + "  0 : Exit Member Search Menu\n" //goto memberMenu
						 + "  1 : Member ID\n"
						 + "  2 : Member Last Name");
		System.out.print("Enter selection: ");
	}//end memberSearchMenu
	
	public static void memberEditMenu() {
		System.out.println("\n----->Member Update Menu\n\n"
						 + "Select member update criteria:\n"
						 + "  0 : Exit Member Update Menu\n" //goto memberMenu
						 + "  1 : Member First Name\n"
						 + "  2 : Member Middle Name\n"
						 + "  3 : Member Last Name\n"
						 + "  4 : Member Address\n"
						 + "  5 : Member Phone Number\n"
						 + "  6 : Set Member's Current Balance");
		System.out.print("Enter selection: ");
	}//end authorEditMenu
	
	public static void rentalMenu() {
		System.out.println("\n----->Rental Menu\n\n"
						 + "Make a selection:\n"
						 + "  0 : Exit Rental Menu\n" //goto mainMenu
						 + "  1 : Search Rentals\n" //goto rentalSearchMenu
						 + "  2 : Check In Book\n" //goto rentalCheckInMenu
						 + "  3 : Check Out Book");
		System.out.print("Enter selection: ");
	}//end memberMenu
	
	public static void rentalSearchMenu() {
		System.out.println("\n----->Rental Search Menu\n\n"
						 + "Select rental search criteria:\n"
						 + "  0 : Exit Member Search Menu\n" //goto rentalMenu
						 + "  1 : Rental ID\n"
						 + "  2 : Member ID\n"
						 + "  3 : View All Incomplete Rentals");
		System.out.print("Enter selection: ");
	}//end rentalSearchMenu

	public static void rentalCheckInMenu() {
		System.out.println("\n----->Check In Menu\n\n"
						 + "Select check in criteria:\n"
						 + "  0 : Exit Check In Menu\n" //goto rentalMenu
						 + "  1 : Rental ID\n"
						 + "  2 : Book ID");
		System.out.print("Enter selection: ");
	}//end rentalCheckInMenu
	
	public static void authorMenu() {
		System.out.println("\n----->Author Menu\n\n"
						 + "Make a selection:\n"
						 + "  0 : Exit Author Menu\n" //goto mainMenu
						 + "  1 : Search Authors\n" //goto authorSearchMenu
						 + "  2 : View All Authors\n"
						 + "  3 : Add New Author to System\n"
						 + "  4 : Edit Existing Author\n" //goto authorEditMenu
						 + "  5 : Add Author to ISBN");
		System.out.print("Enter selection: ");
	}//end authorMenu
	
	public static void authorSearchMenu() {
		System.out.println("\n----->Author Search Menu\n\n"
						 + "Select author search criteria:\n"
						 + "  0 : Exit Author Search Menu\n" //goto authorMenu
						 + "  1 : Author ID\n"
						 + "  2 : Author Last Name");
		System.out.print("Enter selection: ");
	}//end authorSearchMenu
	
	public static void authorEditMenu() {
		System.out.println("\n----->Author Edit Menu\n\n"
						 + "Select author edit criteria:\n"
						 + "  0 : Exit Author Edit Menu\n" //goto authorMenu
						 + "  1 : Author First Name\n"
						 + "  2 : Author Middle Name\n"
						 + "  3 : Author Last Name");
		System.out.print("Enter selection: ");
	}//end authorEditMenu
	
}//end class
